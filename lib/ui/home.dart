import 'package:flutter/material.dart';
import 'package:quizzqpp/model/question.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentQuestion = 0;
  List _questions = [
    Question.name("Texte de la question 1", true, "img.png"),
    Question.name("Texte de la question 2", false, "img.png"),
    Question.name("Texte de la question 3", true, "img.png"),
    Question.name("Texte de la question 4", false, "img.png"),
    Question.name("Texte de la question 5", true, "img.png"),
  ];

  @override
  Widget build(BuildContext context) {
    final ButtonStyle myButtonStyle = ElevatedButton.styleFrom(
      primary: Colors.blueGrey.shade900,
      padding: EdgeInsets.symmetric(horizontal: 20),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20))
      )
    );
    return Scaffold(
      appBar: AppBar(
        title: Text("My Quizz"),
        centerTitle: true,
        backgroundColor: Colors.greenAccent,
      ),
      backgroundColor: Colors.amber,
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
                child: Image.asset(
              "images/img.png",
              width: 250,
              height: 180,
            )),
            Padding(
                padding: EdgeInsets.all(14.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Colors.blueGrey,
                        style: BorderStyle.solid,
                      )),
                  height: 120.0,
                  child: Center(
                      child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      _questions[_currentQuestion].questionText,
                      style: TextStyle(fontSize: 16, color: Colors.black45),
                      textAlign: TextAlign.center,
                    ),
                  )),
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(onPressed: () => _previousQuestion(),
                child: Icon(Icons.arrow_back,color:Colors.white),
                style: myButtonStyle,),
                ElevatedButton(onPressed: () => _checkAnswer(true,context),
                    child: Text("TRUE"),
                style:myButtonStyle),
                ElevatedButton(onPressed: () => _checkAnswer(false,context),
                    child: Text("FALSE"),
                style: myButtonStyle,),
                ElevatedButton(onPressed: () => _nextQuestion(),
                    child: Icon(Icons.arrow_forward,color:Colors.white),
                style: myButtonStyle,),
              ],
            )
          ],
        ),
      ),
    );
  }

  void _checkAnswer(bool choice,BuildContext context){
    if (choice == _questions[_currentQuestion].isCorrect){
      final snackbar = SnackBar(content: Text("GOOD ANSWER!!!"),
      duration: Duration(milliseconds: 500),
    backgroundColor: Colors.lightBlue,);
      ScaffoldMessenger.of(context).showSnackBar(snackbar);
    }else{
    final snackbar = SnackBar(content: Text("BAD ANSWER!!!"),
    duration: Duration(milliseconds: 500),
    backgroundColor: Colors.lightBlue,);
    ScaffoldMessenger.of(context).showSnackBar(snackbar);
    }
    _nextQuestion();
  }


  void _nextQuestion(){
    setState(() {
      _currentQuestion = (_currentQuestion+1)%_questions.length;
    });
  }

  void _previousQuestion(){
    setState(() {
      _currentQuestion = (_currentQuestion-1)%_questions.length;
    });
  }
}
