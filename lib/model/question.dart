class Question{
  String _questionText;
  bool _isCorrect;
  String _image;

  Question.name(this._questionText,this._isCorrect,this._image);

  String get image => _image;

  bool get isCorrect => _isCorrect;

  String get questionText => _questionText;
}